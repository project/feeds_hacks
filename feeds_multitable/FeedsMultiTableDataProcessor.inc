<?php

/**
 * @file
 * Definition of FeedsMultiTableDataProcessor.
 */

class FeedsMultiTableDataProcessor extends FeedsProcessor {

  protected $current_table;

  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {

    // Count number of created and updated nodes.
    $inserted = $updated = array();
    $expiry_time = $this->expiryTime();

    while ($item = $batch->shiftItem()) {
      foreach ($this->config['tables'] as $table) {
        $this->current_table = $table; // for use in setTargetElement()
        $id = $this->existingItemId($batch, $source, $table);

        if ($id === FALSE || !empty($this->config['update_existing'][$table])) {
          // Map item to a data record, feed_nid and timestamp are mandatory.
          $data = array();
          $data['feed_nid'] = $source->feed_nid;
          $data = $this->map($batch, $data);
          if (!isset($data['timestamp'])) {
            $data['timestamp'] = FEEDS_REQUEST_TIME;
          }

          // Only save if this item is not expired.
          if ($expiry_time != FEEDS_EXPIRE_NEVER && $data['timestamp'] < (FEEDS_REQUEST_TIME - $expiry_time)) {
            continue;
          }

          // Save data.
          if ($id !== FALSE) {
            $data = array_merge($data, $id);
            $this->handler($table)->update($data, array_keys($id));
            $updated[$table]++;
          }
          else {
            $this->handler($table)->insert($data);
            $inserted[$table]++;
          }
        }
      }
    }

    foreach ($this->config['tables'] as $table) {
      // Set messages.
      if (!empty($inserted[$table])) {
        drupal_set_message(format_plural($inserted[$table], 'Created @number item in @table.', 'Created @number items in @table.', array('@number' => $inserted[$table], '@table' => $table)));
      }
      if (!empty($updated[$table])) {
        drupal_set_message(format_plural($updated[$table], 'Updated @number item in @table.', 'Updated @number items in @table.', array('@number' => $updated[$table], '@table' => $table)));
      }
      if (empty($inserted[$table]) && empty($updated[$table])) {
        drupal_set_message(t('There are no new items in @table.', array('@table' => $table)));
      }
    }
  }

  /**
   * Implementation of FeedsProcessor::clear().
   *
   * Delete all data records for feed_nid in this table.
   */
  public function clear(FeedsBatch $batch, FeedsSource $source) {
    foreach ($this->config['tables'] as $table) {
      $clause = array();
      if (isset($this->handler($table)->table_schema['fields']['feed_nid'])) {
        $clause['feed_nid'] = $source->feed_nid;
      }
      $num = $this->handler($table)->delete($clause);
      drupal_set_message(format_plural($num, 'Deleted @number record from @table.', 'Deleted @number records from @table.', array('@number' => $num, '@table' => $table)));
    }
  }

  /**
   * Implement expire().
   */
  public function expire($time = NULL) {
    if ($time === NULL) {
      $time = $this->expiryTime();
    }
    if ($time == FEEDS_EXPIRE_NEVER) {
      return FEEDS_BATCH_COMPLETE;
    }
    $clause = array(
      'timestamp' => array(
        '<',
        FEEDS_REQUEST_TIME - $time,
      ),
    );
    foreach ($this->config['tables'] as $table) {
      $num = $this->handler($table)->delete($clause);
      drupal_set_message(format_plural($num, 'Expired @number record from @table.', 'Expired @number records from @table.', array('@number' => $num, '@table' => $table)));
    }
    return FEEDS_BATCH_COMPLETE;
  }

  /**
   * Return expiry time.
   */
  public function expiryTime() {
    return $this->config['expire'];
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $fields = array();
    foreach ($this->config['tables'] as $table_name) {
      $table = data_get_table($table_name);
      $schema = $table->get('table_schema');
      $meta = $table->get('meta');

      // Collect all existing fields except id and field_nid and offer them as
      // mapping targets.
      $existing_fields = array();
      if (isset($schema['fields'])) {
        foreach ($schema['fields'] as $field_name => $field) {
          // Any existing field can be optionally unique.
          // @todo Push this reverse mapping of spec to short name into data
          // module.
          $type = $field['type'];
          if ($type == 'int' && $field['unsigned']) {
            $type = 'unsigned int';
          }
          $existing_fields["$table_name.$field_name"] = array(
            'name' => $table_name .'.'. (empty($meta['fields'][$field_name]['label']) ? $field_name : $meta['fields'][$field_name]['label']),
            'description' => t('Field of type !type.', array('!type' => $type)),
            'optional_unique' => TRUE,
          );
        }
      }
/*
      // Do the same for every joined table.
      foreach ($this->handler($table_name)->joined_tables as $joined_table) {
        $schema = data_get_table($joined_table)->get('table_schema');
        if (isset($schema['fields'])) {
          foreach ($schema['fields'] as $field_name => $field) {
            // Fields in joined tables can't be unique.
            $type = $field['type'];
            if ($type == 'int' && $field['unsigned']) {
              $type = 'unsigned int';
            }
            $existing_fields["$joined_table.$field_name"] = array(
              'name' => $joined_table .'.'. (empty($meta['fields'][$field_name]['label']) ? $field_name : $meta['fields'][$field_name]['label']),
              'description' => t('Joined field of type !type.', array('!type' => $type)),
              'optional_unique' => FALSE,
            );
          }
        }
      }
*/
      drupal_alter('feeds_data_processor_targets', $existing_fields, $table_name);
      $fields += $existing_fields;
    }
    return $fields;
  }

  /**
   * Set target element, bring element in a FeedsDataHandler format.
   */
  public function setTargetElement(&$target_item, $target_element, $value) {
    if (strpos($target_element, '.')) {

      /**

      Add field in FeedsDataHandler format.

      This is the tricky part, FeedsDataHandler expects an *array* of records
      at #[joined_table_name]. We need to iterate over the $value that has
      been mapped to this element and create a record array from each of
      them.
      */
      list($table, $field) = explode('.', $target_element);
      if (in_array($table, $this->config['tables'])) {
        if ($table == $this->current_table) {
          $target_item[$field] = $value;
        }
      }
      else {
        $values = array();
        $value = is_array($value) ? $value : array($value);
        foreach ($value as $v) {
          // Create a record array.
          $values[] = array(
            $field => $v,
          );
        }
        $target_item['#'. $table] = $values;
      }
    }
    else {
      $target_item[$target_element] = $value;
    }
  }

  /**
   * Iterate through unique targets and try to load existing records.
   * Return id for the first match.
   */
  protected function existingItemId($batch, $source, $table) {
    $clauses = array();
    foreach ($this->uniqueTargets($batch) as $target => $value) {
      if (strpos($target, '.')) {
        list($target_table, $target_field) = explode('.', $target);
        if ($target_table != $table) continue;
        $target = $target_field;
      }
      $clauses[$target] = $value;
    }
    if (empty($this->config['globally_unique'])) {
      $clauses['feed_nid'] = $source->feed_nid;
    }
    if ($records = $this->handler($table)->load($clauses)) {
      $id = array_intersect_key($records[0], $clauses);
      return $id;
    }
    return FALSE;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'update_existing' => FEEDS_SKIP_EXISTING,
      'expire' => FEEDS_EXPIRE_NEVER, // Don't expire items by default.
      'mappings' => array(),
      'tables' => array(),
      'globally_unique' => FALSE,
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $period = drupal_map_assoc(array(FEEDS_EXPIRE_NEVER, 3600, 10800, 21600, 43200, 86400, 259200, 604800, 604800 * 4, 604800 * 12, 604800 * 24, 31536000), 'feeds_format_expire');
    $form['expire'] = array(
      '#type' => 'select',
      '#title' => t('Expire items'),
      '#options' => $period,
      '#description' => t('Select after how much time data records should be deleted. The timestamp target value will be used for determining the item\'s age, see Mapping settings.'),
      '#default_value' => $this->config['expire'],
    );
    foreach (data_get_all_tables() as $table_name => $table) {
      $tables[$table_name] = $table->get('title');
    }
    $form['update_existing'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Replace existing records'),
      '#options' => $tables,
      '#description' => t('If an existing record is found for an imported record in one of the selected tables, replace it. Existing records will be determined using mappings that are a "unique target".'),
      '#default_value' => $this->config['update_existing'],
    );
    $form['globally_unique'] = array(
      '#type' => 'checkbox',
      '#title' => t('Globally unique targets'),
      '#description' => t('Check this ON to consider unique targets across all feeds, not just the current one.'),
      '#default_value' => $this->config['globally_unique'],
    );
    $form['tables'] = array(
      '#type' => 'select',
      '#title' => t('Target tables'),
      '#options' => $tables,
      '#multiple' => TRUE,
      '#default_value' => $this->config['tables'],
      '#description' => t('Select the table(s) used as targets for this importer.'),
    );
    return $form;
  }

  /**
   * Return a data handler for this table.
   */
  protected function handler($table) {
    data_include('DataHandler');
    return DataHandler::instance($table);
  }
}

